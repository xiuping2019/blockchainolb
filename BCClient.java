/**
 * Created by sy250140 on 7/28/19.
 */

import java.util.Scanner;

public class BCClient {

    public static void main(String[] args) {
        if (args.length != 3) {
            return;
        }

        int localPort = 0;
        int remotePort = 0;
        String remoteHost = null;

        try {
            localPort = Integer.parseInt(args[0]);
            remoteHost = args[1];
            remotePort = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            return;
        }

        try {
            Scanner sc = new Scanner(System.in);
            while (true) {
                String message = sc.nextLine();
                new Thread(new BCMsgSender(remoteHost, remotePort, message)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}