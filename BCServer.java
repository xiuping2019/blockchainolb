/**
 * Created by sy250140 on 7/28/19.
 */
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;

public class BCServer {

    public static void main(String[] args) {
        if (args.length != 3) {
            return;
        }

        int localPort = 0;
        int remotePort = 0;
        String remoteHost = null;

        try {
            localPort = Integer.parseInt(args[0]);
            remoteHost = args[1];
            remotePort = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            return;
        }
        System.out.println("BC server started:");

        // get block chain
        BankBlockchain theChain = new BankBlockchain();

        TransferData genesisData = new TransferData();
        genesisData.setAmount(0);
        genesisData.setFromAccount("");
        genesisData.setToAccount("");
        genesisData.setDateString("");
        BankBlock genesisBlock  = new BankBlock(genesisData, SHAHashUtil.getSHA256("genesis"));
        theChain.addBlock(genesisBlock);

        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(localPort);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                new Thread(new BCReceiver(clientSocket, remoteHost, remotePort, localPort)).start();

            }
        } catch (IllegalArgumentException e) {
        } catch (IOException e) {
        } finally {
            try {
                if (serverSocket != null)
                    serverSocket.close();
            } catch (IOException e) {
            }
        }
    }
}
