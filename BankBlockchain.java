import java.util.ArrayList;

/**
 * Created by sy250140 on 7/30/19.
 */
public class BankBlockchain {
    private static BankBlock lastBlock;
    private static int length;

    public void setLastBlock(BankBlock lastBlock) {
        this.lastBlock = lastBlock;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public BankBlockchain() {
        length = 0;
    }

    public BankBlock getLastBlock() {
        return this.lastBlock;
    }

    public void addBlock(BankBlock newBlock){
       length++;

       if (lastBlock != null)
           newBlock.setPreviousHash(lastBlock.getCurrentHash());
       else{
           // first genesis block
           newBlock.setPreviousHash(SHAHashUtil.getSHA256("genesis"));
       }
       lastBlock = newBlock;
    }
}
