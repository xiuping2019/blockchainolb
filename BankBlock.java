import java.security.MessageDigest;
import java.util.Date;

/**
 * Created by sy250140 on 7/30/19.
 */
public class BankBlock {
//    private int index;
    private TransferData transferData;
    private String currentHash;
    private String previousHash;

    public boolean isValidBlock(BankBlock theBlock) {
        boolean result = true;
        return result;
    }

    public BankBlock(TransferData transferData, String previousHash) {
//        this.index = index;
        this.transferData = transferData;
        this.currentHash = SHAHashUtil.getSHA256(transferData.getFromAccount()+transferData.getToAccount()+
        transferData.getAmount()+transferData.getDateString());
        this.previousHash = previousHash;
    }

//    public int getIndex() {
//        return index;
//    }
//
//    public void setIndex(int index) {
//        this.index = index;
//    }

    public TransferData getTransferData() {
        return transferData;
    }

    public void setTransferData(TransferData transferData) {
        this.transferData = transferData;
    }

    public String getCurrentHash() {
        return currentHash;
    }

    public void setCurrentHash(String currentHash) {
        this.currentHash = currentHash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }
}
