/**
 * Created by sy250140 on 7/28/19.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class BCMsgSender implements Runnable{

    private String remoteHost;
    private int remotePort;
    private String message;

    public BCMsgSender(String remoteHost, int remotePort, String message) {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        this.message = message;
    }

    @Override
    public void run() {
        try {
            // create socket with a timeout of 2 seconds
            Socket s = new Socket();
            s.connect(new InetSocketAddress(this.remoteHost, this.remotePort), 20000);
            System.out.println(" Sending to remote host name is " + this.remoteHost);
            System.out.println(" Sending to remote port is " + this.remotePort);

            PrintWriter pw =  new PrintWriter(s.getOutputStream(), true);

            System.out.println("sending out message is:  " + message);
            // send the message forward
            pw.println(message);
            pw.flush();

            // close printWriter and socket
            pw.close();
            s.close();

        } catch (IOException e) {
//            System.out.println("something is wrong during client sending info");
 //           System.out.println(e.getMessage());
//            System.out.println(e.getCause());
//            System.out.println(e.getStackTrace());
        }
    }
}