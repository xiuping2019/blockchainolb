
/**
 * Created by sy250140 on 7/28/19.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
public class BCReceiver implements Runnable{

    private Socket toClient;
    private String remoteHost;
    private int remotePort;
    private int localPort;

    public BCReceiver (Socket toClient, String remoteHost, int remotePort, int localPort) {
        this.toClient = toClient;
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        this.localPort = localPort;
    }

    @Override
    public void run() {
        try {
            serverHandler(toClient.getInputStream());
            toClient.close();
        } catch (IOException e) {
        }
    }

    public void serverHandler(InputStream clientInputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientInputStream));

        try {
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                System.out.println("receiving the incoming message is: " + line);

                String remoteIP = (((InetSocketAddress) toClient.getRemoteSocketAddress()).getAddress()).toString().replace("/", "");

                System.out.println("remoteIP is " + remoteIP);
                // check message types

                String[] tokens = line.split("\\|");
                int amount=0;
                String fromAccount ="";
                String toAccount ="";
                String dateString="";
                String currentHash="";
                String prevHash="";

                switch (tokens[0]) {
                    case "tx":
                        fromAccount = tokens[1];
                        toAccount = tokens[2];
                        amount = Integer.parseInt(tokens[3]);
                        dateString = tokens[4];
                        System.out.println("Transfer received:  " + line);
                        System.out.println("    From account: "+fromAccount);
                        System.out.println("    To account: "+toAccount);
                        System.out.println("    Transfer amount: "+amount);
                        System.out.println("    Transfer date: "+dateString);
                        break;
                    case "bc":
                        // newly created block being forwarded here
                        fromAccount = tokens[1];
                        toAccount = tokens[2];
                        amount = Integer.parseInt(tokens[3]);
                        dateString = tokens[4];
                        currentHash = tokens[5];
                        prevHash = tokens[6];
                        System.out.println("New Block from peer received: " + line);
                        System.out.println(" Block index :  " + tokens[1] );
                        System.out.println("    From account: "+fromAccount);
                        System.out.println("    To account: "+toAccount);
                        System.out.println("    Transfer amount: "+amount);
                        System.out.println("    Transfer date: "+dateString);
                        System.out.println(" Current Hash :  " + currentHash );
                        System.out.println(" Previous Block's Hash :  " + prevHash );
                        break;
                    default:
                }

                if (tokens[0].equals("bc")){
                    BankBlockchain theChain = new BankBlockchain();

                    BankBlock lastBlock =  theChain.getLastBlock();

                    TransferData theData = new TransferData();
                    theData.setAmount(amount);
                    theData.setFromAccount(fromAccount);
                    theData.setToAccount(toAccount);
                    theData.setDateString(dateString);

                    BankBlock newBlock = new BankBlock(theData ,prevHash);
                    if(isValid(newBlock, currentHash)) {
                        theChain.addBlock(newBlock);
                        System.out.println("Newly received block is valid and is replicated locally successfully. ");
                    }
                    else{
                        // invalid
                        // send back rejection message
                        System.out.println("remote host is " + remoteHost);
                        System.out.println("Original port is " + remotePort);
                        if (remotePort == 9001)
                            remotePort =9002;

                        if (remotePort == 8011)
                            remotePort = 8012;
                        System.out.println(" port is " + remotePort);

                        Thread thread = new Thread(new BCMsgSender(remoteHost, remotePort, "The forwarded block from peer is not valid, reject it."));
                        thread.start();
                    }
                }

                if (tokens[0].equals("tx")){
                    BankBlockchain theChain = new BankBlockchain();

                    BankBlock lastBlock =  theChain.getLastBlock();

                    TransferData theData = new TransferData();
                    theData.setAmount(amount);
                    theData.setFromAccount(fromAccount);
                    theData.setToAccount(toAccount);
                    theData.setDateString(dateString);

                    BankBlock newBlock = new BankBlock(theData ,lastBlock.getCurrentHash());
                    theChain.addBlock(newBlock);
                    String forwardMessage = "A new block of transfer transaction just got added to the bankblockchain successfully.";
                    System.out.println(forwardMessage);
                    // Add new block to blockchain, broadcast the content of the new block;
                    this.broadcast("bc|"+fromAccount+"|"+toAccount+"|"+amount+"|"+dateString+"|"+newBlock.getCurrentHash()+"|"+newBlock.getPreviousHash(), remoteIP, 9002);
                }
            }
        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            System.out.println(e.getStackTrace().toString());
        }
    }

    public void broadcast(String message, String remoteHost, int remotePort) {
        System.out.println("Broadcast to remote host: " +remoteHost);
        System.out.println("Broadcast to remote port: " +remotePort);
         Thread thread = new Thread(new BCMsgSender(remoteHost, remotePort, message));
        thread.start();
    }

    public boolean isValid(BankBlock newBlock, String currentHash){
        boolean result = false;
        TransferData theData = newBlock.getTransferData();
        if (SHAHashUtil.getSHA256(theData.getFromAccount()+ theData.getToAccount()+ theData.getAmount()+theData.getDateString()).equals(currentHash)){
            result = true;
        }
        return result;
    }
}
